
#load

makeJSONNetworkFile <- function(location, fileName, edgeHash, nodeHash){
  
  sink(paste(location,fileName,sep=""))
  cat('var graph = { "nodes": [')  
  
  allNodes <- keys(nodeHash)
  numNodes <- length(allNodes)
  thisIndex <- 1
  for (node in allNodes){
    
    nodeString <- paste('{ "name": ', paste('"',node,'"',sep=""),
                        ', "group": ', nodeHash[[node]] '}',
                        sep="" )
    
    if(thisIndex == numNodes){
      nodeString <- paste(nodeString, '}', sep="" )
    }else{
      nodeString <- paste(nodeString, '},', sep="" )
    }
    
    thisIndex <- thisIndex + 1
    cat(nodeString)
    
  }
  
  cat('], \n')
  cat('"links": [')
  
  edgeHashKeys <- keys(edgeHash)
  numOfKeys <- length(edgeHashKeys)
  edgeIndex <- 1
  for (e in edgeHashKeys){
    
    score <-  round(as.numeric(edgeHash[[e]]),2)
    nodes <- splitEdgeKey(e)
    n1 <- nodes[1]
    n2 <- nodes[2]
    thisEdgeString <- paste('{ "source": ', paste('"',n1,'"',sep=""),
                            ',"target": ',  paste('"',n2,'"',sep=""),
                            ', "score": ',score
                            , sep="" )
    
    if(edgeIndex==numOfKeys){
      thisEdgeString <- paste(thisEdgeString, '}', sep="" )
    }else{
      thisEdgeString <- paste(thisEdgeString, '},', sep="" )
    }
    
    edgeIndex <- edgeIndex + 1
    
    cat(thisEdgeString)
  }
  
  cat(']  \n }; \n')
  sink()
  
}

#for taibo

for(i in seq(50,5050,by=500)){
  
  print(i)
  
  subgraph <- erdos.renyi.game(1000, 0.3, type=c("gnp", "gnm"),directed = FALSE, loops = FALSE)
  
  V(subgraph)$label <- paste("node_",seq(1, vcount(subgraph)),sep="")
  V(subgraph)$indexInG <- seq(1, vcount(subgraph))
 
  comm <- fastgreedy.community(subgraph)
  V(subgraph)$commMbr <- comm$membership
  
  allEdges <- hash()
  allGenes <- hash()
  
  for(i in 1: vcount(subgraph)){
    thisComm <- get.vertex.attribute(subgraph,"commMbr",index = i)
    thisName <- get.vertex.attribute(subgraph,"label",index = i)
    .set(allGenes,thisName, thisComm) 
  }
  
  edgeList <- get.edgelist(subgraph)
  rows <- nrow(edgeList)
  
  if(rows>0){
    for(k in 1:rows){
      
      n1Index <- edgeList[k,1]
      n2Index <- edgeList[k,2]
      n1 <- get.vertex.attribute(g,"label",index = V(subgraph)$indexInG[n1Index])
      n2 <- get.vertex.attribute(g,"label",index = V(subgraph)$indexInG[n2Index])
      
      edgeKey <- getEdgeKey(n1,n2)
      score <- get.edge.attribute(subgraph, "weight", index=k)
      .set(allEdges,edgeKey,score)
      
    } 
  } 
  
  fileName <- paste("sim_", i, ".js",sep="")
  makeJSONNetworkFile("location_to_save", fileName, allEdges, allGenes)
  
}