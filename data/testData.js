var jsonData =  
{
  "nodes":  [

    // comm 1
    { "name": "A"},
    { "name": "B"},
    { "name": "C"},
    { "name": "D"},
    { "name": "E"},
 
     // comm 2
    { "name": "F"},
    { "name": "G"},
    { "name": "H"},
    { "name": "I"},
    { "name": "J"},
 

  ],

  "links":  [

    // comm 1
    { "source":  1,  "target":  0,  "score":  1 },
    { "source":  2,  "target":  0,  "score":  1 },
    { "source":  3,  "target":  0,  "score":  1 },
    { "source":  4,  "target":  0,  "score":  1 },  

 
    { "source":  2,  "target":  1,  "score":  1 },
    { "source":  3,  "target":  1,  "score":  1 },
    { "source":  4,  "target":  1,  "score":  1 }, 
 
    { "source":  3,  "target":  2,  "score":  1 },
    { "source":  4,  "target":  2,  "score":  1 }, 
 
    { "source":  4,  "target":  3,  "score":  1 },  

    // comm 2
    { "source":  6,  "target":  5,  "score":  1 },
    { "source":  7,  "target":  5,  "score":  1 },
    { "source":  8,  "target":  5,  "score":  1 },
    { "source":  9,  "target":  5,  "score":  1 },  

 
    { "source":  7,  "target":  6,  "score":  1 },
    { "source":  8,  "target":  6,  "score":  1 },
    { "source":  9,  "target":  6,  "score":  1 }, 
 
    { "source":  8,  "target":  7,  "score":  1 },
    { "source":  9,  "target":  7,  "score":  1 }, 
 
    { "source":  9,  "target":  8,  "score":  1 },

    //comm1 <--> comm2
    { "source":  3,  "target":  5,  "score":  1 },
    // { "source":  4,  "target":  6,  "score":  1 } 


  ]
};