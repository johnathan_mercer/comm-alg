

function comm(nodes, edges){

	//compute communities here and return the nodes with the new annotation

	//original paper
	//http://www.ece.unm.edu/ifis/papers/community-moore.pdf

	// discussion
	//http://stackoverflow.com/questions/20443351/clauset-newman-moore-community-detection-implementation

	//node
	// { name: , index: , degree: , community: }

	var A = []; // adjacency matrix
	var m = edges.length; // number of edges in the graph
	var Q = 0; // [ratio of within to between]  - [expected ratio of with to between]
	var comm = []; // holds the current communities
	var aForComm = []; // holds a_i's for communities
	var deltaQ = []; //holds the increase in modularity from merging each comm i,j
	
	var c = 1 / (2 * m);

	// to keep track of the max deltaQ and the respective indices i,j
	var maxDeltaQ = - Number.MAX_VALUE;
	var maxDeltaQ_i = 0;
	var maxDeltaQ_j = 0;
	
	// index array, to conveniently store indices 
	// note javascript is zero-indexed
	var indexArray = [];
	for (var i = 0; i <= nodes.length-1; i++) {
		indexArray.push(i);
	}
	
	// helper function to tell array membership
	function contains(a, obj) {
		var i = a.length;
		while (i--) {
			if (a[i] === obj) {
				return true;
			}
		}
		return false;
	}

	function delta(i, j){
		return (i==j) ? 1 : 0;
	}

	function e(comm_i,comm_j){

		//community i and j
		var e_ij = 0;

		// sum_vw(A_vw * delta(c_v, i)*delta(c_w,j))
		var nodesi = nodes.filter(function(x,i){return x.community == comm_i;});
		var nodesj = nodes.filter(function(x,i){return x.community == comm_j;});

		_.each(nodesi, function(x,i){
			_.each(nodesj, function(y,j){
				//don't really need the delta's since we are subsetting to the comm members
				e_ij += A[x.index][y.index] * delta(x.community, comm_i) * delta(x.community, comm_j);
			})
		})

		return c * e_ij ;
	}

	function a(comm_i){

		var sum_v = 0;

		_.each(nodes, function(x,i){
				sum_v += x.degree * delta(x.community, comm_i)
		})

		// (1/2m) * sum_v( k_v * delta(c_v, i)) 
		return c * sum_v;
	}

	function getQ(){

		var sum = 0;

		_.each(comm, function(x,i){

			var e_ii = e(i,i);
			var a_ii = Math.pow(a(i),2);
			sum += (e_ii - a_ii);

		})

		return sum;
	}

      var scores = [];
      var nodeHash = {};
      _.each(nodes, function(x,i){ x.index = i;
      							   x.community = i;
      							   x.degree = 0;
      							   scores[i]=[]; 
      							   comm[i] = [];
      							   comm[i].push(x.name); // init all nodes in their own comm
      							   nodeHash[x.name] = x;
      							 })
 
	  _.each(edges, function(x,i){
	        if(x.source.name != x.target.name){
		        //increment degree
		        x.source.degree +=1;
		        x.target.degree +=1;
		        scores[x.source.index][x.target.index] = x.score;
		        scores[x.target.index][x.source.index] = x.score;

	        }
	  })

      //set adjacency matrix A, deltaQ, and aForComm
      _.each(nodes, function(x) { 

        if(typeof A[x.index] == 'undefined') A[x.index] = [];
		if(typeof deltaQ[x.index] == 'undefined') deltaQ[x.index] = []; // initialize deltaQ using (8) in the paper
		aForComm[x.index] = c * x.degree; //init aForComm using (9) in the paper
		
        _.each(nodes, function(y) { 

        							if(typeof scores[x.index][y.index] !== 'undefined' && x.index!==y.index)
        							{
        								A[x.index][y.index] = 1;
        								
										var updatedDeltaQ = c - x.degree*y.degree*(Math.pow(c,2));
										
										deltaQ[x.index][y.index] = updatedDeltaQ;

										// TODO: generalize this to add edge weights										
										// updating maximum deltaQ and indices
										if(updatedDeltaQ > maxDeltaQ){
											maxDeltaQ = updatedDeltaQ;
											maxDeltaQ_i = x.index;
											maxDeltaQ_j = y.index;
										}
										
        							}else{
        								A[x.index][y.index] = 0;
										deltaQ[x.index][y.index] = 0;
        							}
        							  
    	});
      
      });

	var copyA = aForComm;
    Q = getQ(); 

	_.each(indexArray, function(x){
		_.each(indexArray, function(y){
			if(deltaQ[x][y] > maxDeltaQ && x!=y){
				maxDeltaQ = deltaQ[x][y];
				maxDeltaQ_i = x;
				maxDeltaQ_j = y;
			}
		})
	})

	var somePositive = (maxDeltaQ > 0); // deltaQ_ij has positive elements
	var iteration = 0;
	while(somePositive){

		// merge the two communities indexed by maxDeltaQ_i and maxDeltaQ_j, 
		// and labelling the combined community as maxDeltaQ_j
		
		// update node properties
		var nodesi = nodes.filter(function(x,i){return x.community == maxDeltaQ_i;});
		_.each(nodesi, function(y){
			y.community = maxDeltaQ_j;		
		})
		
		// update comm array, assuming it stores the list of gene names in the community
		// this is not needed for the current implementation 
		comm[maxDeltaQ_j] = comm[maxDeltaQ_j].concat(comm[maxDeltaQ_i]);
		delete comm[maxDeltaQ_i];		
		

		//update deltaQ_ij
		
		var commsIndex_i = indexArray.filter(function(x){return deltaQ[x][maxDeltaQ_i] !=0 ;});
		var commsIndex_j = indexArray.filter(function(y){return deltaQ[y][maxDeltaQ_j] !=0 ;});

		_.each(indexArray, function(k){
			if(k!=maxDeltaQ_i && k!=maxDeltaQ_j){ 
				if(contains(commsIndex_i, k) && contains(commsIndex_j, k)){
					// this is equation (10a) in the paper
					var updatedDeltaQ = (deltaQ[k][maxDeltaQ_i]+deltaQ[k][maxDeltaQ_j]);
					deltaQ[maxDeltaQ_j][k] = updatedDeltaQ;
					deltaQ[k][maxDeltaQ_j] = updatedDeltaQ;
				}else if(contains(commsIndex_i, k)){
					// this is equation (10b) in the paper
					var updatedDeltaQ = (deltaQ[k][maxDeltaQ_i]-aForComm[maxDeltaQ_j]*aForComm[k]);	
					deltaQ[maxDeltaQ_j][k] = updatedDeltaQ;
					deltaQ[k][maxDeltaQ_j] = updatedDeltaQ;					
				}else if(contains(commsIndex_j, k)){
					// this is equation (10c) in the paper
					var updatedDeltaQ = (deltaQ[k][maxDeltaQ_j]-aForComm[maxDeltaQ_i]*aForComm[k]);
					deltaQ[maxDeltaQ_j][k] = updatedDeltaQ;
					deltaQ[k][maxDeltaQ_j] = updatedDeltaQ;	
				}

			} 
		})

		// deletions
		
		_.each(indexArray, function(x){
			if(deltaQ[x][maxDeltaQ_i] != null){
				delete deltaQ[x][maxDeltaQ_i];
			}
			if(deltaQ[maxDeltaQ_i][x] != null){
				delete deltaQ[maxDeltaQ_i][x];
			}
		})
		
		delete indexArray[maxDeltaQ_i]; // leaving only indices for remaining communities

		// update aForComm
		aForComm[maxDeltaQ_j]+=aForComm[maxDeltaQ_i];
		aForComm[maxDeltaQ_i]=0;

		maxDeltaQ = - Number.MAX_VALUE;
		// update maxDeltaQ, this may be time consuming without the data structures used in the paper
		_.each(indexArray, function(x){
			_.each(indexArray, function(y){
				if(deltaQ[x][y] > maxDeltaQ && x!=y){
					maxDeltaQ = deltaQ[x][y];
					maxDeltaQ_i = x;
					maxDeltaQ_j = y;
				}
			})

		})

		somePositive = (maxDeltaQ > 0);
	}

	return nodes;
}