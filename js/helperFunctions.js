var zoom = d3.behavior.zoom()
                .scaleExtent([0.1,5])
                .on("zoom", redraw);

function redraw() {
  svg.attr("transform","translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
}